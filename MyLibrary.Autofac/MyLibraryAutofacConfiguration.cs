﻿using Autofac;
using MyLibrary.Bootstrap;

namespace MyLibrary.Autofac
{
    public static class MyLibraryAutofacConfiguration
    {
        public static ContainerBuilder AddMyLibrary(this ContainerBuilder containerBuilder)
        {
            var myLibraryDependencyContainer = new MyLibraryDependencyContainer(containerBuilder);
            MyLibraryDependencyConfiguration.Register(myLibraryDependencyContainer);

            return containerBuilder;
        }
    }
}
