﻿using Autofac;
using MyLibrary.Bootstrap.Interfaces;

namespace MyLibrary.Autofac
{
    public class MyLibraryDependencyContainer : IMyLibraryDependencyContainer
    {
        private readonly ContainerBuilder _containerBuilder;

        public MyLibraryDependencyContainer(ContainerBuilder containerBuilder) =>
            _containerBuilder = containerBuilder;

        public void RegisterScoped<TInterface, TImplementation>()
            where TImplementation : class, TInterface
            where TInterface : class =>
            _containerBuilder.RegisterType<TImplementation>().As<TInterface>().InstancePerLifetimeScope();

        public void RegisterSingleton<TInterface, TImplementation>()
            where TImplementation : class, TInterface
            where TInterface : class =>
            _containerBuilder.RegisterType<TImplementation>().As<TInterface>().SingleInstance();

        public void RegisterTransient<TInterface, TImplementation>()
            where TImplementation : class, TInterface
            where TInterface : class =>
            _containerBuilder.RegisterType<TImplementation>().As<TInterface>().InstancePerDependency();
    }
}
