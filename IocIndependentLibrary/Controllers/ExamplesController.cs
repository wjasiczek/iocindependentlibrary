﻿using Microsoft.AspNetCore.Mvc;
using MyLibrary.Services.Interfaces;
using System;

namespace IocIndependentLibrary.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExamplesController : ControllerBase
    {
        private readonly IMyFirstService _myFirstService;
        private readonly IMySecondService _mySecondService;

        public ExamplesController(
            IMyFirstService myFirstService, 
            IMySecondService mySecondService)
        {
            _myFirstService = myFirstService;
            _mySecondService = mySecondService;
        }

        [HttpGet()]
        public IActionResult Get() =>
            Ok($"{_myFirstService.HelloWorld()}{Environment.NewLine}" +
                $"{_mySecondService.HelloWorld()}");
    }
}
