﻿using MyLibrary.Services.Interfaces;

namespace MyLibrary.Services
{
    public class MyFirstService : IMyFirstService
    {
        public string HelloWorld() =>
            $"HelloWorld from {nameof(MyFirstService)}";
    }
}
