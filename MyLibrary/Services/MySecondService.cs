﻿using MyLibrary.Services.Interfaces;

namespace MyLibrary.Services
{
    public class MySecondService : IMySecondService
    {
        public string HelloWorld() =>
            $"HelloWorld from {nameof(MySecondService)}";
    }
}
