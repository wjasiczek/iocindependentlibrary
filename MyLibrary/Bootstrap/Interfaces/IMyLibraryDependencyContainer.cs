﻿namespace MyLibrary.Bootstrap.Interfaces
{
    public interface IMyLibraryDependencyContainer
    {
        void RegisterSingleton<TInterface, TImplementation>()
            where TImplementation : class, TInterface
            where TInterface : class;
        void RegisterScoped<TInterface, TImplementation>()
            where TImplementation : class, TInterface
            where TInterface : class;
        void RegisterTransient<TInterface, TImplementation>()
            where TImplementation : class, TInterface
            where TInterface : class;
    }
}
