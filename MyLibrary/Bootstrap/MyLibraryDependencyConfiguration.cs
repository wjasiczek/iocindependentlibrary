﻿using MyLibrary.Bootstrap.Interfaces;
using MyLibrary.Services;
using MyLibrary.Services.Interfaces;

namespace MyLibrary.Bootstrap
{
    public static class MyLibraryDependencyConfiguration
    {
        public static void Register(IMyLibraryDependencyContainer myLibraryDependencyContainer)
        {
            myLibraryDependencyContainer.RegisterScoped<IMyFirstService, MyFirstService>();
            myLibraryDependencyContainer.RegisterTransient<IMySecondService, MySecondService>();
        }
    }
}
