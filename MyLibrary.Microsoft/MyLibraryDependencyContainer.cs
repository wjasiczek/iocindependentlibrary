﻿using Microsoft.Extensions.DependencyInjection;
using MyLibrary.Bootstrap.Interfaces;

namespace MyLibrary.Microsoft
{
    public class MyLibraryDependencyContainer : IMyLibraryDependencyContainer
    {
        private readonly IServiceCollection _serviceCollection;

        public MyLibraryDependencyContainer(IServiceCollection serviceCollection) =>
            _serviceCollection = serviceCollection;

        void IMyLibraryDependencyContainer.RegisterScoped<TInterface, TImplementation>() =>
            _serviceCollection.AddScoped<TInterface, TImplementation>();

        void IMyLibraryDependencyContainer.RegisterSingleton<TInterface, TImplementation>() =>
            _serviceCollection.AddSingleton<TInterface, TImplementation>();

        void IMyLibraryDependencyContainer.RegisterTransient<TInterface, TImplementation>() =>
            _serviceCollection.AddTransient<TInterface, TImplementation>();
    }
}
