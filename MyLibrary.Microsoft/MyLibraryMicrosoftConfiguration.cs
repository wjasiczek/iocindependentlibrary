﻿using Microsoft.Extensions.DependencyInjection;
using MyLibrary.Bootstrap;

namespace MyLibrary.Microsoft
{
    public static class MyLibraryMicrosoftConfiguration
    {
        public static IServiceCollection AddMyLibrary(this IServiceCollection serviceCollection)
        {
            var myLibraryDependencyContainer = new MyLibraryDependencyContainer(serviceCollection);
            MyLibraryDependencyConfiguration.Register(myLibraryDependencyContainer);

            return serviceCollection;
        }
    }
}
